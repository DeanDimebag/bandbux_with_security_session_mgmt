package com.dipesh.bandbux.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.BookingDetails;
import com.dipesh.bandbux.entities.StatusName;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.BookingDetailsRepository;
import com.dipesh.bandbux.repositories.StatusNameRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import com.dipesh.bandbux.services.BookingPdfServiceImpl;
import com.dipesh.bandbux.util.EmailServiceUtil;
import com.dipesh.bandbux.util.PdfGeneratorUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/booking")
public class BookingDetailsController extends CRUDController<BookingDetails, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingDetailsController.class);

    @Autowired
    private BookingDetailsRepository bookingDetailsRepository;

    @Autowired
    private StatusNameRepository statusNameRepository;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private BookingPdfServiceImpl bookingPdfServiceImpl;

    @Autowired
    private PdfGeneratorUtil pdfGeneratorUtil;

    @Autowired
    private EmailServiceUtil sendEmailServiceUtil;

    public BookingDetailsController(){
        pageIndex = "All Booking Details";
        pageTitle = "Band BUX | All Booking";
        pageURI = "booking";
        viewPath = "booking";
    }

    @PostMapping("/upload")
    @ResponseBody
    public String saveBooking(BookingDetails bookingDetails, HttpSession session) {
        LOGGER.info("Inside saveBooking().");
        StatusName statusName = new StatusName();
        statusName.setStatusName("Pending");
        StatusName savedStatusName = statusNameRepository.save(statusName);
        List<StatusName> statusNames = new ArrayList<>();
        statusNames.add(savedStatusName);
        bookingDetails.setStatusName(statusNames);
        BookingDetails savedBookingDetails = bookingDetailsRepository.save(bookingDetails);
        LOGGER.info("Booked Succesfully");
        
//        String filePath = "/Users/Dipesh/bookingPDF/bands/booking"+savedBookingDetails.getBookId()+".pdf";
//        pdfGeneratorUtil.pdfgenerate(savedBookingDetails, filePath);
//        
//        String from = (String) session.getAttribute("email");
//        LOGGER.info("From Email : "+from);
//
//        User user = userRepository.findByName(bookingDetails.getBandName());
//        String to = user.getEmail();
//        LOGGER.info("To Email : "+to);
//
//		String subject = "Details of your Booking.";
//		String body ="Please Find Your Attachment.";
//        
//        sendEmailServiceUtil.sendMailFromTo(from, to, subject, body, filePath);
        return "success";
    }
   
}