package com.dipesh.bandbux.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/organizers")
public class OrganizerController extends CRUDController<User, Integer>{

    private static final Logger LOGGER = LoggerFactory.getLogger(OrganizerController.class);

    @Autowired
    private UserRepository userRepository;

    public OrganizerController(){
        pageTitle = "Band Bux | Organizer";
        pageURI = "organizers";
        viewPath = "organizers";
        pageIndex = "Organizer";
    }

    @GetMapping
    public String showOrganizers(Principal principal, HttpSession session, Model model) {
        LOGGER.info("Inside showOrganizers().");
        String email = principal.getName();
        LOGGER.info("Email : " + email);
        session.setAttribute("email", email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("records", userRepository.findRoleByOrganizer());
        return "organizers/index";
    }

    @GetMapping("/all")
    @ResponseBody
    public List<User> showOrganizer(){
        return userRepository.findRoleByOrganizer();
    }

    @GetMapping("/profile/{id}")
    public String showProfile(HttpSession session, Model model, @PathVariable("id") int organizerID){
        LOGGER.info("Inside showProfile().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : "+email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("organizer", userRepository.findById(organizerID).get());
        return viewPath +"/profile";
    }

    @GetMapping("/profile/json/{id}")
    @ResponseBody
    public User showOrganizer(@PathVariable("id") int organizerID){
        LOGGER.info("Inside showOrganizer");
        return userRepository.findById(organizerID).get();
    }

}