package com.dipesh.bandbux.controller;

import javax.servlet.http.HttpSession;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.BandMember;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.BandMemberRespository;
import com.dipesh.bandbux.repositories.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/bandmember")
public class BandMemberController extends CRUDController<BandMember, Integer>{

    private static final Logger LOGGER = LoggerFactory.getLogger(BandMemberController.class);

    @Autowired
    private BandMemberRespository bandMemberRespository;
    
    public BandMemberController(){
        pageTitle = "Band Bux | Band Member";
        pageIndex = "Band Member";
        pageURI = "bandmember";
        viewPath = "member";
    }

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public String showBandMember(HttpSession session,Model model){
        LOGGER.info("Inside showBandMember().");
        String email = (String) session.getAttribute("email");
        LOGGER.info(("Email : "+email));
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("members", bandMemberRespository.findAll());
        return "bands/member/index";
    }

    @PostMapping ("/upload")
    @ResponseBody
    public String saveBandMember(BandMember bandMember, HttpSession session){
        LOGGER.info("Inside saveBandMember().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : "+email);
        LOGGER.info("Member :"+bandMember.getMemberName());
        User user = userRepository.findByEmail(email);
        bandMember.setUser(user);
        bandMemberRespository.save(bandMember);
        LOGGER.info("Band Member Saved");
        return "success";
    }
    

    @GetMapping("/detail/{id}")
    public String showMemberDetail(@PathVariable("id") int memberID, HttpSession session, Model model){
        LOGGER.info("Inseide showMemberDetail().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : "+email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("member", bandMemberRespository.findById(memberID).get());
        return "/bands/member/detail";
    }

    @GetMapping("/band/{id}")
    @ResponseBody
    public BandMember getBandMemberJson(@PathVariable("id") int bandID){
        return bandMemberRespository.findByBandId(bandID);
    }
}