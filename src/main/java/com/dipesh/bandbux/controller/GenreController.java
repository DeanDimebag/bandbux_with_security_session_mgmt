/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.controller;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.BandGenre;
import com.dipesh.bandbux.entities.Genre;
import com.dipesh.bandbux.repositories.BandGenreRepository;
import com.dipesh.bandbux.repositories.GenreRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping("genres")
public class GenreController extends CRUDController<Genre, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenreController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private BandGenreRepository bandGenreRepository;

    public GenreController() {
        pageTitle = "Band Bux | Genres";
        pageIndex = "Genres";
        pageURI = "genres";
        viewPath = "genres";
    }

    @GetMapping
    public String showGenre(Principal principal, HttpSession session, Model model) {
        LOGGER.info("Inside showGenre().");
        String email = principal.getName();
        LOGGER.info("Email : " + email);
        session.setAttribute("email", email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("records", genreRepository.findAll());
        return viewPath+"/index";
    }

    @PostMapping("/upload")
    @ResponseBody
    @Transactional
    public String uploadGenre(Genre genre){
        LOGGER.info("Inside uploadGenre().");
        genreRepository.save(genre);
        return "success";
    }

    @GetMapping("/unselected/{id}")
    @ResponseBody
    public List<Genre> getUnselectedGenre(@PathVariable("id") int bandID){
        return genreRepository.getGenreNotInBand(bandID);
    }

    @PostMapping("/band-genre/upload")
    @ResponseBody
    public String saveBandGenre(BandGenre bandGenre){
        LOGGER.info("Inside saveBandGenre().");
        LOGGER.info("Band ID : "+bandGenre.getUser().getId());
        LOGGER.info("Genre ID : "+bandGenre.getGenre().getId());
        bandGenreRepository.save(bandGenre);
        LOGGER.info("Genre added by Band");
        return "success";
    }

    @PostMapping("/new-genre/add")
    @ResponseBody
    public String addBandGenre(Genre genre){
        LOGGER.info("Inside addBandGenre().");
        genreRepository.save(genre);
        LOGGER.info("New Genre added");
        return "success";
    }

}
