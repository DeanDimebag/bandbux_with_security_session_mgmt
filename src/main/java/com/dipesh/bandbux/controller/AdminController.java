package com.dipesh.bandbux.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin")
public class AdminController extends CRUDController<User, Integer>{

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private UserRepository userRepository;

    public AdminController(){
        pageTitle = "Band Bux | Admin";
        pageURI = "admin";
        viewPath = "admin";
        pageIndex = "Admin";
    }

    @GetMapping
    public String showAdmin(Principal principal, HttpSession session, Model model) {
        LOGGER.info("Inside showAdmin().");
        String email = principal.getName();
        LOGGER.info("Email : " + email);
        session.setAttribute("email", email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("records", userRepository.findRoleByAdmin());
        return "admin/index";
    }

    @GetMapping("/all")
    @ResponseBody
    public List<User> showAdmins(){
        return userRepository.findRoleByAdmin();
    }

    @GetMapping("/profile/{id}")
    public String showProfile(HttpSession session, Model model, @PathVariable("id") int adminID){
        LOGGER.info("Inside showProfile().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : "+email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("admin", userRepository.findById(adminID).get());
        return viewPath +"/profile";
    }

}