package com.dipesh.bandbux.controller;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.Status;
import com.dipesh.bandbux.repositories.StatusRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import com.dipesh.bandbux.services.StatusServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/status")
public class StatusController extends CRUDController<Status, Integer> {

    public StatusController() {
        pageIndex = "Status";
        pageTitle = "Band Bux | Status";
        pageURI = "status";
        viewPath = "status";
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(StatusController.class);
    private static final String USER_DIR = System.getProperty("user.dir")
            + "/src/main/resources/static/img/band/status";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private StatusServiceImpl statusServiceImpl;

    @GetMapping
    public String showStatus(Principal principal, HttpSession session, Model model) {
        LOGGER.info("Inside showStatus().");
        String email = principal.getName();
        LOGGER.info(("Email : " + email));
        session.setAttribute("email", email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("records", statusRepository.findAll());
        return viewPath + "/index";
    }

    @PostMapping("/upload")
    @ResponseBody
    @Transactional
    public String uploadStatus(@RequestParam("description") String description,
            @RequestParam("imageName") MultipartFile imageName, HttpSession session) {
        LOGGER.info("Inside uploadStatus().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : "+email);
        statusServiceImpl.StatusService(description, imageName, USER_DIR, email);
        return "success";
    }

    @GetMapping("/detail/{id}")
    public String showDetail(@PathVariable("id") int statusID, HttpSession session, Model model) {
        LOGGER.info("Inside showDetail().");
        String email = (String) session.getAttribute("email");
        LOGGER.info(("Email : " + email));
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("status", statusRepository.findById(statusID).get());
        return viewPath + "/detail";
    }

}