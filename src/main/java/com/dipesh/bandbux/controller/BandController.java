package com.dipesh.bandbux.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.Status;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.StatusRepository;
import com.dipesh.bandbux.repositories.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/bands")
public class BandController extends CRUDController<User, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BandController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StatusRepository statusRepository;

    public BandController(){
        pageTitle = "Band Bux | Bands";
        pageURI = "bands";
        viewPath = "bands";
        pageIndex = "Band";
    }
    
    @GetMapping
    public String showBands(Principal principal, HttpSession session, Model model){
        LOGGER.info("Inside showBands().");
        String email = principal.getName();
        LOGGER.info("Email : "+email);
        session.setAttribute("email", email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("records", userRepository.findRoleByBand());
        return viewPath +"/index";
    }

    @GetMapping("/all")
    @ResponseBody
    public List<User> showBands(){
        return userRepository.findRoleByBand();
    }

    @GetMapping("/profile/{id}")
    public String showProfile(HttpSession session, Model model, @PathVariable("id") int bandID){
        LOGGER.info("Inside showProfile().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : "+email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("band", userRepository.findById(bandID).get());
        model.addAttribute("statoos", statusRepository.findStatusByBandId(bandID));
        return viewPath +"/profile";
    }
    
    @GetMapping("/status/json/{id}")
    @ResponseBody
    public List<Status> showProfileJson(HttpSession session, Model model, @PathVariable("id") int bandID){
        LOGGER.info("Inside showProfileJson().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : "+email);
        return statusRepository.findStatusByBandId(bandID);
    }

    @GetMapping("/profile/json/{id}")
    @ResponseBody
    public User showUserProfile(@PathVariable("id") int userID){
        LOGGER.info("Inside showUserProfile().");
        return userRepository.findById(userID).get();
    }
}