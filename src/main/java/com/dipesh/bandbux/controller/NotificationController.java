/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.controller;

import com.dipesh.bandbux.core.controller.SiteController;
import com.dipesh.bandbux.entities.BookingDetails;
import com.dipesh.bandbux.entities.BookingStatus;
import com.dipesh.bandbux.entities.StatusName;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.BookingDetailsRepository;
import com.dipesh.bandbux.repositories.BookingStatusRepository;
import com.dipesh.bandbux.repositories.StatusNameRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping("/notification")
public class NotificationController extends SiteController {

    private static Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);

    public NotificationController() {
        pageTitle = "Band Bux | Notification";
        pageURI = "notification";
        viewPath = "notification";
        pageIndex = "Your Notifications";
    }

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BookingDetailsRepository bookingDetailsRepository;

    @Autowired
    private StatusNameRepository statusNameRepository;

    @Autowired
    private BookingStatusRepository bookingStatusRepository;

    @GetMapping
    public String showNotification(Principal principal, Model model, HttpSession session) {
        LOGGER.info("Inside showNotification().");
        String email = principal.getName();
        LOGGER.info("Email : " + email);
        User user = userRepository.findByEmail(email);
        int userID = user.getId();
        LOGGER.info("ID : " + userID);
        session.setAttribute("email", email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("notifications", bookingDetailsRepository.getAllBandDetails(userID));
        model.addAttribute("notifications", bookingDetailsRepository.getAllOrganizerDetails(userID));
        return "notification";
    }

    @PostMapping("/accept")
    @ResponseBody
    public String acceptBook(@RequestParam("bookId") int bookID) {
        LOGGER.info("Inside acceptBook().");
        StatusName statusName = new StatusName();
        statusName.setStatusName("Accepted");
        StatusName savedStatusName = statusNameRepository.save(statusName);
        List<StatusName> statusNames = new ArrayList<>();
        statusNames.add(savedStatusName);
        LOGGER.info("Status Name Saved.");
        BookingStatus bookingStatus = bookingStatusRepository.getBookStatus(bookID);
        bookingStatus.setStatusNames(statusName);
        bookingStatusRepository.save(bookingStatus);
        LOGGER.info("Accepted.");
        return "success";
    }

    @PostMapping("/decline")
    @ResponseBody
    public String declineBook(@RequestParam("bookId") int bookID) {
        LOGGER.info("Inside declineBook().");
        StatusName statusName = new StatusName();
        statusName.setStatusName("Decline");
        StatusName savedStatusName = statusNameRepository.save(statusName);
        List<StatusName> statusNames = new ArrayList<>();
        statusNames.add(savedStatusName);
        LOGGER.info("Status Name Saved.");
        BookingStatus bookingStatus = bookingStatusRepository.getBookStatus(bookID);
        bookingStatus.setStatusNames(statusName);
        bookingStatusRepository.save(bookingStatus);
        LOGGER.info("Declined.");
        return "success";
    }

}
