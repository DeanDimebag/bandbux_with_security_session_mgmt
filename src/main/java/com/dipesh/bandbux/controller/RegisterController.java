package com.dipesh.bandbux.controller;

import com.dipesh.bandbux.entities.Role;
import com.dipesh.bandbux.entities.Type;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.RoleRepository;
import com.dipesh.bandbux.repositories.TypeRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import com.dipesh.bandbux.util.EmailServiceUtil;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/register")
public class RegisterController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);
    
    @Autowired
    private BCryptPasswordEncoder encoder;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private TypeRepository typeRepository;

    @Autowired
    private EmailServiceUtil emailServiceUtil;
    
    @GetMapping
    public String showRegister(){
        LOGGER.info("Inside showRegister().");
        return "register/index";
    }
    
    
    @PostMapping
    @ResponseBody
    @Transactional
    public String saveUser(@RequestParam("name") String name,@RequestParam("location") String location,
            @RequestParam("phone") String phone,@RequestParam("email") String email,
            @RequestParam("password") String password,@RequestParam("role") String role,
            @RequestParam("type") String type,@RequestParam("status") boolean status){
        LOGGER.info("Inside saveUser().");
         User user = new User();
        user.setName(name);
        user.setLocation(location);
        user.setPhone(phone);
        user.setEmail(email);
        user.setPassword(encoder.encode(password));
        Role userRole = new Role();
        userRole.setName(role);
        Role savedRole = roleRepository.save(userRole);
        List<Role> roles = new ArrayList<>();
        roles.add(savedRole);
        user.setRoles(roles);
        LOGGER.info("Role saved.");
        Type userType = new Type();
        userType.settypeName(type);
        Type savedType = typeRepository.save(userType);
        List<Type> types = new ArrayList<>();
        types.add(savedType);
        user.setTypes(types);
        LOGGER.info("Type saved.");
        user.setStatus(status);
        userRepository.save(user);
        LOGGER.info("User saved.");
//        emailServiceUtil.sendMail(email, "Successfully Register", 
//        "Welcome to our website BANDBUX. You are successfully register. Thank you and enjoy.");
//        LOGGER.info("email send");
        return "success";
    }
    
}