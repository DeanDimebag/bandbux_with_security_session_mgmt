package com.dipesh.bandbux.controller;

import com.dipesh.bandbux.core.controller.SiteController;
import com.dipesh.bandbux.entities.ProfilePicture;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.BandStatusRepository;
import com.dipesh.bandbux.repositories.ProfilePictureRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import com.dipesh.bandbux.util.ImageUploadServiceUtil;
import java.io.IOException;

import java.security.Principal;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/")
public class HomeController extends SiteController {

    public HomeController() {
        pageTitle = "Band Bux | Home";
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterController.class);

    private static final String USER_DIR = System.getProperty("user.dir")
            + "/src/main/resources/static/img/user/pp";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BandStatusRepository bandStatusRepository;

    @Autowired
    private ImageUploadServiceUtil imageUploadServiceUtil;

    @Autowired
    private ProfilePictureRepository profilePictureRepository;

    @GetMapping
    public String showHome(Principal principal, HttpSession session, Model model) {
        LOGGER.info("Inside showHome().");
        String email = principal.getName();
        LOGGER.info("Email : " + email);
        session.setAttribute("email", email);
        model.addAttribute("user", userRepository.findByEmail(email));
        return "index";
    }

    @GetMapping("/profile/{id}")
    public String showProfile(@PathVariable("id") int userID, Model model) {
        LOGGER.info("Inside showProfile().");
        model.addAttribute("user", userRepository.findById(userID).get());
        model.addAttribute("statoos", bandStatusRepository.findStatusByBandId(userID));
        return "profile";
    }

    // @PostMapping("/upload")
    // @ResponseBody
    // @Transactional
    // public String uploadImage(@RequestParam("description") String description,
    // @RequestParam("imageName") MultipartFile imageName){
    // LOGGER.info("Inside uploadImage().");
    // LOGGER.info("Description : "+ description);
    // LOGGER.info("Image Name : "+imageName);
    // return "success";
    // }
    @PostMapping("/img/upload")
    @ResponseBody
    @Transactional
    public String uploadProfilePicture(@RequestParam("profilePicture") MultipartFile profilePicture, Principal principal) {
        LOGGER.info("Inside uploadProfilePicture().");
        LOGGER.info("Image Name : " + profilePicture);
        imageUploadServiceUtil.UploadImage(USER_DIR, profilePicture);
        String pictureName = profilePicture.getOriginalFilename();
        String email = principal.getName();
        LOGGER.info("Email : " + email);
        User user = userRepository.findByEmail(email);
        ProfilePicture pp = new ProfilePicture();
        pp.setPpName(pictureName);
        pp.setUser(user);
        profilePictureRepository.save(pp);
        LOGGER.info("Profile Picture Saved.");
        return "success";
    }

}
