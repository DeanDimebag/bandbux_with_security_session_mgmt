package com.dipesh.bandbux.controller;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.OrganizerPost;
import com.dipesh.bandbux.entities.Post;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.OrganizerPostRepository;
import com.dipesh.bandbux.repositories.PostRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import com.dipesh.bandbux.util.ImageUploadServiceUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/organizer-post")
public class OrganizerPostController extends CRUDController<OrganizerPost, Integer>{

    private static final Logger LOGGER = LoggerFactory.getLogger(OrganizerPostController.class);

    private static final String USER_DIR = System.getProperty("user.dir")
            + "/src/main/resources/static/img/organizer/post";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrganizerPostRepository organizerPostRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private ImageUploadServiceUtil imageUploadServiceUtil;

    public OrganizerPostController(){
        pageTitle = "Band Bux | Organizer Post";
        pageIndex = "Organizer Post";
        pageURI = "organizer-post";
        viewPath = "organizer-post";
    }

    @GetMapping
    public String showOrganizerPost(Principal principal ,HttpSession session, Model model){
        LOGGER.info("Inside showOrganizerPost().");
        String email = principal.getName();
        LOGGER.info("Email : "+email);
        session.setAttribute("email", email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("posts", organizerPostRepository.findAll());
        return "/organizers/organizer-post/index";
    }

    @PostMapping
    @ResponseBody
    @Transactional
    public String saveOrganizerPost(@RequestParam("description") String description,
    @RequestParam("imageName") MultipartFile imageName, HttpSession session){
        LOGGER.info("Inside saveOrganizerPost().");
        String email = (String) session.getAttribute("email");
        User user = userRepository.findByEmail(email);
        Post post = new Post();
        post.setPostDescription(description);
        imageUploadServiceUtil.UploadImage(USER_DIR, imageName);
        String pictureName = imageName.getOriginalFilename().toString();
        post.setPictureName(pictureName);
        Post savedpost = postRepository.save(post);
        LOGGER.info("Post saved");
        OrganizerPost OP = new OrganizerPost();
        OP.setUser(user);
        OP.setPost(savedpost);
        organizerPostRepository.save(OP);
        LOGGER.info("Organizer post saved.");
        return "success";
    }

    @GetMapping("/post/{id}")
    @ResponseBody
    public OrganizerPost showPost(@PathVariable("id") int postID) {
        return organizerPostRepository.findPostByPostId(postID);
    }

}