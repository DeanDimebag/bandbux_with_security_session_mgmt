/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.controller;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.UserRepository;
import java.security.Principal;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Dipesh
 */
@Controller
@RequestMapping("/users")
public class UserController extends CRUDController<User, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepository;

    public UserController() {
        pageTitle = "Band Bux | Users";
        pageURI = "users";
        viewPath = "users";
        pageIndex = "User";
    }

    @GetMapping
    public String showUserList(Principal principal, HttpSession session, Model model) {
        LOGGER.info("Inside showUserList().");
        String email = principal.getName();
        LOGGER.info("Email : " + email);
        session.setAttribute("email", email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("records", userRepository.findAll());
        return "users/index";
    }

    @GetMapping("/profile/{id}")
    public String showProfile(HttpSession session, Model model, @PathVariable("id") int bandID) {
        LOGGER.info("Inside showProfile().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : " + email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("users", userRepository.findById(bandID).get());
        return viewPath + "/profile";
    }

    @GetMapping("/profile/json/{id}")
    @ResponseBody
    public User getUserJson(@PathVariable("id") int userID) {
        return userRepository.findById(userID).get();
    }

}
