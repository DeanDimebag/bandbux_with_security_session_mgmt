package com.dipesh.bandbux.controller;

import javax.servlet.http.HttpSession;

import com.dipesh.bandbux.core.controller.CRUDController;
import com.dipesh.bandbux.entities.BandDetail;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.BandDetailRepository;
import com.dipesh.bandbux.repositories.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/band-detail")
public class BandDetailController extends CRUDController<BandDetail, Integer>{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(BandDetailController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BandDetailRepository bandDetailRepository;

    public BandDetailController(){
        pageIndex = "Band Detail";
        pageTitle = "Band Bux | Band Detail";
        pageURI = "band-detail";
        viewPath = "band-detail";
    }

    @GetMapping
    public String showBandDetails(HttpSession session, Model model){
        LOGGER.info("Inside showBandDetails().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : "+email);
        model.addAttribute("user", userRepository.findByEmail(email));
        model.addAttribute("details", bandDetailRepository.findAll());
        return "bands/band-detail/index";
    }

    @PostMapping
    @ResponseBody
    @Transactional
    public String saveDetails(BandDetail bandDetail, HttpSession session){
        LOGGER.info("Inside saveDetails().");
        String email = (String) session.getAttribute("email");
        LOGGER.info("Email : "+email);
        User user = userRepository.findByEmail(email);
        bandDetail.setUser(user);
        bandDetailRepository.save(bandDetail);
        LOGGER.info("Band Detail saved.");
        return "success";
    }

    @GetMapping("/detail/{id}")
    @ResponseBody
    public BandDetail showDetail(@PathVariable ("id") int bandID){
        return bandDetailRepository.findDetailByBandId(bandID);
    }

}