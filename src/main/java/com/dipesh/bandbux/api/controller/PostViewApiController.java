/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.api.controller;

import com.dipesh.bandbux.entities.Post;
import com.dipesh.bandbux.entities.PostView;
import com.dipesh.bandbux.repositories.PostViewRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Dipesh
 */
@RestController
@RequestMapping("/api")
public class PostViewApiController {
    
    @Autowired
    private PostViewRepository postViewRepository;
    
    @GetMapping("/posts")
    public List<PostView> getPost(){
       return postViewRepository.findAll();
    }
}
