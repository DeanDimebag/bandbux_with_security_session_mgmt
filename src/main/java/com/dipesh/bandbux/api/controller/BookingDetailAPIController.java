/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.api.controller;

import com.dipesh.bandbux.entities.BookingDetails;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.BookingDetailsRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Dipesh
 */
@RestController
@RequestMapping("/api")
public class BookingDetailAPIController {

    @Autowired
    private BookingDetailsRepository bookingDetailsRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/booking/band/details")
    @ResponseBody
    public List<BookingDetails> getBandDetailJson(Principal principal) {
        String email = principal.getName();
        User user = userRepository.findByEmail(email);
        int userID = user.getId();
        return bookingDetailsRepository.getAllBandDetails(userID);
    }
    
    @GetMapping("/booking/organizer/details")
    @ResponseBody
    public List<BookingDetails> getOrganizerDetailJson(Principal principal) {
        String email = principal.getName();
        User user = userRepository.findByEmail(email);
        int userID = user.getId();
        return bookingDetailsRepository.getAllOrganizerDetails(userID);
    }

    @GetMapping("/booking/details/pending/{id}")
    @ResponseBody
    public List<BookingDetails> getPendingDetailJson(@PathVariable("id") int userID) {
        return bookingDetailsRepository.getAllDetailIfPending(userID);
    }

    @GetMapping("/booking/details/accepted/{id}")
    @ResponseBody
    public List<BookingDetails> getAcceptedDetailJson(@PathVariable("id") int userID) {
        return bookingDetailsRepository.getAllDetailIfAccepted(userID);
    }

    @GetMapping("/booking/details/decline/{id}")
    @ResponseBody
    public List<BookingDetails> getDeclineDetailJson(@PathVariable("id") int userID) {
        return bookingDetailsRepository.getAllDetailIfDecline(userID);
    }

    @GetMapping("/booking/details/status/read")
    @ResponseBody
    public List<BookingDetails> getReadDetailJson(Principal principal) {
        String email = principal.getName();
        User user = userRepository.findByEmail(email);
        int userID = user.getId();
        return bookingDetailsRepository.getAllDetailsIfTrue(userID);
    }

    @GetMapping("/booking/details/status/unread")
    @ResponseBody
    public List<BookingDetails> getUnreadDetailJson(Principal principal) {
        String email = principal.getName();
        User user = userRepository.findByEmail(email);
        int userID = user.getId();
        return bookingDetailsRepository.getAllDetailsIfFalse(userID);
    }
}
