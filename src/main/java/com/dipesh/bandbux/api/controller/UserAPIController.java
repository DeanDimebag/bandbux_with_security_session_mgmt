/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.api.controller;

import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Dipesh
 */
@RestController
@RequestMapping("/api")
public class UserAPIController {
    
    @Autowired
    private UserRepository userRepository;
    
    @GetMapping("/profile/{id}")
    @ResponseBody
    public User showProfileJson(@PathVariable("id") int userID){
        return userRepository.findById(userID).get();
    }
    
    @GetMapping("/allUser")
    @ResponseBody
    public List<User> showAllUser(){
        return userRepository.findAll();
    }
}
