package com.dipesh.bandbux.api.controller;

import java.util.List;

import com.dipesh.bandbux.entities.OrganizerPost;
import com.dipesh.bandbux.repositories.OrganizerPostRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class OrganizerPostAPIController {

    @Autowired
    private OrganizerPostRepository organizerPostRepository;

    // All Post with organizer detail
    @GetMapping("/organizer/post")
    public List<OrganizerPost> showPostAndUser() {
        return organizerPostRepository.findPostAndUser();
    }

    //Specific list of organizer's posts with user details
    @GetMapping("/organizer/post/{id}")
    public List<OrganizerPost> showOrganizerPost(@PathVariable("id") int organizerID) {
        return organizerPostRepository.findPostByOrganizerId(organizerID);
    }
}