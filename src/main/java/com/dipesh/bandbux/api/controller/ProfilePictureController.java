package com.dipesh.bandbux.api.controller;

import java.util.List;

import com.dipesh.bandbux.entities.ProfilePicture;
import com.dipesh.bandbux.repositories.ProfilePictureRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProfilePictureController {

    @Autowired
    private ProfilePictureRepository profilePictureRepository;

    @GetMapping("/pp/{id}")
    public List<ProfilePicture> getProfilePicture(@PathVariable("id") int userID){
        return profilePictureRepository.findByUserId(userID);
    }

    @GetMapping("/latest/pp/{id}")
    public ProfilePicture getLatestProfilePicture(@PathVariable("id") int userID){
        return profilePictureRepository.findLatestProfileByUserId(userID);
    }
}