package com.dipesh.bandbux.api.controller;

import java.util.List;

import com.dipesh.bandbux.entities.BandStatus;
import com.dipesh.bandbux.entities.Status;
import com.dipesh.bandbux.repositories.BandStatusRepository;
import com.dipesh.bandbux.repositories.StatusRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class BandApiController {
    
    @Autowired
    private BandStatusRepository bandStatusRepository;

    @Autowired
    private StatusRepository statusRepository;

    
    //status of band with user details
    @GetMapping("/status/band/{id}")
    public List<BandStatus> getBandStatus(@PathVariable("id") int BandID){
        return bandStatusRepository.findStatusByBandId(BandID);
    }

    //status of band without user details
    @GetMapping("/statusOnly/band/{id}")
    public List<Status> getBandStatusOnly(@PathVariable("id") int BandID){
        return statusRepository.findStatusByBandId(BandID);
    }

    //All Band's status
    @GetMapping("/band/status")
    public List<BandStatus> showStatusAndUser() {
        return bandStatusRepository.findStatusAndUser();
    }
    
}