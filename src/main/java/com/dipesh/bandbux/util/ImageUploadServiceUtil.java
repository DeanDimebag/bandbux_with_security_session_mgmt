package com.dipesh.bandbux.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ImageUploadServiceUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageUploadServiceUtil.class);
    
    public void UploadImage(String UPLOAD_DIR, MultipartFile pictureName) {
        LOGGER.info("Inside UploadImage().");
        StringBuilder builder = new StringBuilder();
        Path fileNameAndPath = Paths.get(UPLOAD_DIR, pictureName.getOriginalFilename());
        builder.append(pictureName.getOriginalFilename());
        try {
            Files.write(fileNameAndPath, pictureName.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String image = pictureName.getOriginalFilename().toString();
        LOGGER.info("Picture Name : "+image);
    }

}