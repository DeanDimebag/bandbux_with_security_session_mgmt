package com.dipesh.bandbux.util;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceUtil.class);

    @Value("${spring.mail.username}")
    private String mailAddress;

    @Autowired
    private JavaMailSender sender;

    public void sendMail(String email, String subject, String body) {
        LOGGER.info("Email Address : " + mailAddress);
        MimeMessagePreparator preparator = (MimeMessage message) -> {
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setFrom("chajichat123@gmail.com");
            helper.setTo(email);
            helper.setSubject(subject);
            helper.setText(body);
        };
        this.sender.send(preparator);
    }

    public void sendMailFromTo(String from, String to, String subject, String body, String filepath) {
        MimeMessagePreparator preparator = (MimeMessage message) -> {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(body);
            LOGGER.info("all set.");
            helper.addAttachment("Booking", new File(filepath));
            LOGGER.info("Pdf set.");
        };
        this.sender.send(preparator);
        LOGGER.info("Mail send.");
    }

}