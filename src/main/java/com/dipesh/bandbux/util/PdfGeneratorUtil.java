package com.dipesh.bandbux.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.dipesh.bandbux.entities.BookingDetails;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.springframework.stereotype.Component;

@Component
public class PdfGeneratorUtil {
    public void pdfgenerate(BookingDetails bookingDetails, String filePath) {
        Document document = new Document();

        try {
            PdfWriter.getInstance(document, new FileOutputStream(filePath));
            document.open();
            document.add(generateTable(bookingDetails));
            document.close();
        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        }
    }

    private PdfPTable generateTable(BookingDetails bookingDetails) {

        PdfPTable table = new PdfPTable(2);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("Band Bux"));
        cell.setColspan(2);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Booking Details :"));
        cell.setColspan(2);
        table.addCell(cell);

        table.addCell("Band Name");
        table.addCell(bookingDetails.getBandName());
        table.addCell("Venue Name");
        table.addCell(bookingDetails.getVenueName());
        table.addCell("Event Date");
        table.addCell(bookingDetails.getBookDate().toString());
        table.addCell("Start Time");
        table.addCell(bookingDetails.getStartTime());
        table.addCell("Edn Time");
        table.addCell(bookingDetails.getEndTime());
        return table;
    }
}