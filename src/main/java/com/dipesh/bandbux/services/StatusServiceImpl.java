package com.dipesh.bandbux.services;

import com.dipesh.bandbux.entities.BandStatus;
import com.dipesh.bandbux.entities.Status;
import com.dipesh.bandbux.entities.User;
import com.dipesh.bandbux.repositories.BandStatusRepository;
import com.dipesh.bandbux.repositories.StatusRepository;
import com.dipesh.bandbux.repositories.UserRepository;
import com.dipesh.bandbux.util.ImageUploadServiceUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class StatusServiceImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageUploadServiceUtil.class);

    @Autowired
    private ImageUploadServiceUtil imageUploadServiceUtil;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private BandStatusRepository bandStatusRepository;


    public void StatusService(String description,MultipartFile imageName,String USER_DIR, String email){
        Status status = new Status();
        status.setStatusDescription(description);
        imageUploadServiceUtil.UploadImage(USER_DIR, imageName);
        String pictureName = imageName.getOriginalFilename().toString();
        status.setPictureName(pictureName);
        User user = userRepository.findByEmail(email);
        LOGGER.info("Band ID : "+user.getId());
        Status saveStatus = statusRepository.save(status);
        LOGGER.info("Status ID : "+saveStatus.getId());
        BandStatus bandStatus = new BandStatus();
        bandStatus.setUser(user);
        bandStatus.setStatus(saveStatus);
        bandStatusRepository.save(bandStatus);
    }
}