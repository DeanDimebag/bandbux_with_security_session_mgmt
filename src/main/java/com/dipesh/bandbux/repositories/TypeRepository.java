/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.repositories;

import com.dipesh.bandbux.entities.Type;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Dipesh
 */
public interface TypeRepository extends JpaRepository<Type, Integer>{
    
}
