package com.dipesh.bandbux.repositories;

import com.dipesh.bandbux.entities.BandMember;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BandMemberRespository extends JpaRepository<BandMember, Integer>{

    @Query(value = "Select * from tbl_band_member where band_id = ?", nativeQuery = true)
	public BandMember findByBandId(int memberID);
    
}