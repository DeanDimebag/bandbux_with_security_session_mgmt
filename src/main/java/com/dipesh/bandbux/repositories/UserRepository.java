/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.repositories;

import com.dipesh.bandbux.entities.Post;
import java.util.List;

import com.dipesh.bandbux.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Dipesh
 */
public interface UserRepository extends JpaRepository<User, Integer>{

    public User findByEmail(String email);

    @Query(value = "SELECT * FROM tbl_users AS U JOIN tbl_user_role as UR ON U.id=UR.user_id JOIN tbl_user_type as UT ON U.id= UT.user_id where role_name = 'ROLE_ADMIN'",nativeQuery = true)
    public List<User> findRoleByAdmin();

    @Query(value = "SELECT * FROM tbl_users AS U JOIN tbl_user_role as UR ON U.id=UR.user_id JOIN tbl_user_type as UT ON U.id= UT.user_id where role_name = 'ROLE_BAND'",nativeQuery = true)
    public List<User> findRoleByBand();
    
    @Query(value = "SELECT * FROM tbl_users AS U JOIN tbl_user_role as UR ON U.id=UR.user_id JOIN tbl_user_type as UT ON U.id= UT.user_id where role_name = 'ROLE_ORGANIZER'",nativeQuery = true)
    public List<User> findRoleByOrganizer();

    public User findByName(String bandName);        
}
