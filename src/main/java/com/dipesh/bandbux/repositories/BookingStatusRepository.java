package com.dipesh.bandbux.repositories;

import com.dipesh.bandbux.entities.BookingStatus;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookingStatusRepository extends JpaRepository<BookingStatus, Integer>{
    
    @Query(value = "select * from tbl_booking_status where book_id = ?", nativeQuery = true)
    public BookingStatus getBookStatus(int id);
}