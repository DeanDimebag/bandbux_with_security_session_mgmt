package com.dipesh.bandbux.repositories;

import com.dipesh.bandbux.entities.StatusName;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusNameRepository extends JpaRepository<StatusName, Integer>{
    
}