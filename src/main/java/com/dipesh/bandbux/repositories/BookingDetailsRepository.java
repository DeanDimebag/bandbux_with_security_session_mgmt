package com.dipesh.bandbux.repositories;

import com.dipesh.bandbux.entities.BookingDetails;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookingDetailsRepository extends JpaRepository<BookingDetails, Integer> {

    @Query(value = "select * from tbl_booking_detail as BD "
            + "join tbl_booking_status as BS on BD.booking_id=BS.book_id "
            + "join tbl_users as U on U.name= BD.band_name where U.id = ?", nativeQuery = true)
    List<BookingDetails> getAllBandDetails(int id);
    
    @Query(value = "select * from tbl_booking_detail as BD "
            + "join tbl_booking_status as BS on BD.booking_id=BS.book_id "
            + "join tbl_users as U on U.name= BD.venue_name where U.id = ?", nativeQuery = true)
    List<BookingDetails> getAllOrganizerDetails(int id);

    @Query(value = "select * from tbl_booking_detail as BD "
            + "join tbl_booking_status as BS on BD.booking_id=BS.book_id "
            + "join tbl_users as U on U.name= BD.band_name where U.id = ? and BD.status=true", nativeQuery = true)
    List<BookingDetails> getAllDetailsIfTrue(int id);

    @Query(value = "select * from tbl_booking_detail as BD "
            + "join tbl_booking_status as BS on BD.booking_id=BS.book_id "
            + "join tbl_users as U on U.name= BD.band_name where U.id = ? and BD.status=false", nativeQuery = true)
    List<BookingDetails> getAllDetailsIfFalse(int id);

    @Query(value = "select * from tbl_booking_detail as BD "
            + "join tbl_booking_status as BS on BD.booking_id=BS.book_id "
            + "join tbl_users as U on U.name= BD.band_name where U.id = ? and stat_name = 'Accepted';", nativeQuery = true)
    List<BookingDetails> getAllDetailIfAccepted(int id);

    @Query(value = "select * from tbl_booking_detail as BD "
            + "join tbl_booking_status as BS on BD.booking_id=BS.book_id "
            + "join tbl_users as U on U.name= BD.band_name where U.id = ? and stat_name = 'Pending';", nativeQuery = true)
    List<BookingDetails> getAllDetailIfPending(int id);
    
    @Query(value = "select * from tbl_booking_detail as BD "
            + "join tbl_booking_status as BS on BD.booking_id=BS.book_id "
            + "join tbl_users as U on U.name= BD.band_name where U.id = ? and stat_name = 'Decline';", nativeQuery = true)
    List<BookingDetails> getAllDetailIfDecline(int id);
}
