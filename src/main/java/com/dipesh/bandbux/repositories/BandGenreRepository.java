package com.dipesh.bandbux.repositories;

import com.dipesh.bandbux.entities.BandGenre;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BandGenreRepository extends JpaRepository<BandGenre, Integer>{
    
}