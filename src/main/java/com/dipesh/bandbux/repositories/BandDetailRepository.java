package com.dipesh.bandbux.repositories;

import com.dipesh.bandbux.entities.BandDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BandDetailRepository extends JpaRepository<BandDetail, Integer>{
    @Query(value = "select * from tbl_band_details where band_id = ?", nativeQuery = true)
    public BandDetail findDetailByBandId(int id);
}