package com.dipesh.bandbux.repositories;

import java.util.List;

import com.dipesh.bandbux.entities.ProfilePicture;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProfilePictureRepository extends JpaRepository<ProfilePicture, Integer>{

    @Query(value = "select * from tbl_profile_picture where user_id = ?", nativeQuery = true)
	public List<ProfilePicture> findByUserId(int userID);

    @Query(value = "SELECT id, user_id, update_date, pp_name FROM tbl_profile_picture"
    +" WHERE update_date=(SELECT MAX(update_date) FROM tbl_profile_picture WHERE user_id=?)", nativeQuery = true)
	public ProfilePicture findLatestProfileByUserId(int userID);
    
}