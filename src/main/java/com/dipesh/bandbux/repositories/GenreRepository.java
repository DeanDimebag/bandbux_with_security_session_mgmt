/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.repositories;

import java.util.List;

import com.dipesh.bandbux.entities.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Dipesh
 */
public interface GenreRepository extends JpaRepository<Genre, Integer>{
    @Query(value = "select * from tbl_genres where id not in(select genre_id from tbl_band_genre where band_id=?)", nativeQuery = true)
        public List<Genre> getGenreNotInBand(int id);
}
