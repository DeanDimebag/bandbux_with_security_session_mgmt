package com.dipesh.bandbux.repositories;

import java.util.List;

import com.dipesh.bandbux.entities.BandStatus;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BandStatusRepository extends JpaRepository<BandStatus, Integer>{
    @Query(value = "select * from tbl_band_status as BS join tbl_status as S on BS.s_id = S.status_id "
    +" join tbl_users as U on BS.band_id = U.id where band_id = ?", nativeQuery = true)
	public List<BandStatus> findStatusByBandId(int bandID);

    @Query(value = "select * from tbl_status as S join tbl_band_status as BS on S.status_id=BS.s_id", nativeQuery = true)
	public List<BandStatus> findStatusAndUser();
}