package com.dipesh.bandbux.repositories;

import com.dipesh.bandbux.entities.UserType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserTypeRepositry extends JpaRepository<UserType, Integer>{
    
}