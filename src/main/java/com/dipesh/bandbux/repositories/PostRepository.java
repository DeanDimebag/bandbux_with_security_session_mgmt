package com.dipesh.bandbux.repositories;

import java.util.List;

import com.dipesh.bandbux.entities.Post;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PostRepository extends JpaRepository<Post, Integer>{
    @Query(value = "select * from tbl_organizer_post as OP join tbl_posts as P on OP.p_id = P.post_id"
    +" join tbl_users as U on OP.organizer_id = U.id where organizer_id = ?", nativeQuery = true)
    List<Post> findPostByOrganizerId(int organizerID);
    
    @Query(value = "select * from tbl_organizer_post as OP join tbl_posts as P on OP.p_id = P.post_id"
    +" join tbl_users as U on OP.organizer_id = U.id where p_id = ?", nativeQuery = true)
	public Post findPostByPostId(int postID);
}