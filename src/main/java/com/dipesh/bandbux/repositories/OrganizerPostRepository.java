package com.dipesh.bandbux.repositories;

import java.util.List;

import com.dipesh.bandbux.entities.OrganizerPost;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrganizerPostRepository extends JpaRepository<OrganizerPost, Integer>{
    @Query(value = "select * from tbl_organizer_post as OP join tbl_posts as P on OP.p_id = P.post_id"
    +" join tbl_users as U on OP.organizer_id = U.id where organizer_id = ?", nativeQuery = true)
    public List<OrganizerPost> findPostByOrganizerId(int organizerID);
    
    @Query(value = "select * from tbl_organizer_post as OP join tbl_posts as P on OP.p_id = P.post_id"
    +" join tbl_users as U on OP.organizer_id = U.id where p_id = ?", nativeQuery = true)
    public OrganizerPost findPostByPostId(int postID);
    
    @Query(value = "select * from tbl_posts as P join tbl_organizer_post as OP on P.post_id=OP.p_id", nativeQuery = true)
	public List<OrganizerPost> findPostAndUser();
}