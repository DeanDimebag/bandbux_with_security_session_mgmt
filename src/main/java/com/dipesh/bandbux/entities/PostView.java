/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Dipesh
 */

@Entity
@Table(name = "tbl_all_posts")
public class PostView {
    
    @Id
    @Column(name = "id")
    private int id;
    
    @Column(name = "name")
    private String name; 
    
    @Column(name = "post_id")
    private int postId;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "image_name")
    private String imageName;
    
    @Column(name = "postdate")
    private Date postDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    @Override
    public String toString() {
        return "PostView{" + "id=" + id + ", name=" + name + ", postId=" + postId + ", description=" + description + ", imageName=" + imageName + ", postDate=" + postDate + '}';
    }
    
}
