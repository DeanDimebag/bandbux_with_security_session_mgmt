/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dipesh.bandbux.entities;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Dipesh
 */
@Entity
@Table(name = "tbl_users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "location")
    private String location;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "entry_date", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;

    @Column(name = "status")
    private boolean status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "tbl_user_role", joinColumns = {
            @JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
                    @JoinColumn(name = "role_name", referencedColumnName = "name") })
    private List<Role> roles;

    @ManyToMany
    @JoinTable(name = "tbl_user_type", joinColumns = {
            @JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
                    @JoinColumn(name = "type_name", referencedColumnName = "name") })
    private List<Type> types;

    @JoinTable(name = "tbl_band_genre", joinColumns = {
            @JoinColumn(name = "band_id", referencedColumnName = "id") }, inverseJoinColumns = {
                    @JoinColumn(name = "genre_id", referencedColumnName = "id") })
    @ManyToMany
    private List<Genre> genre;

    @OneToOne(mappedBy = "user")
    private BandDetail bandDetail;

    @OneToOne(mappedBy = "user")
    private BandMember bandMember;

    @ManyToMany
    @JoinTable(name = "tbl_organizer_post", joinColumns = {
            @JoinColumn(name = "organizer_id", referencedColumnName = "id") }, inverseJoinColumns = {
                    @JoinColumn(name = "p_id", referencedColumnName = "post_id") })
    private List<Post> posts;

    @JoinTable(name = "tbl_band_status", joinColumns = {
            @JoinColumn(name = "band_id", referencedColumnName = "id") }, inverseJoinColumns = {
                    @JoinColumn(name = "s_id", referencedColumnName = "status_id") })
    @ManyToMany
    private List<Status> statoos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    public List<Genre> getGenre() {
        return genre;
    }

    public void setGenre(List<Genre> genre) {
        this.genre = genre;
    }

    public BandMember getBandMember() {
        return bandMember;
    }

    public void setBandMember(BandMember bandMember) {
        this.bandMember = bandMember;
    }

    public BandDetail getBandDetail() {
        return bandDetail;
    }

    public void setBandDetail(BandDetail bandDetail) {
        this.bandDetail = bandDetail;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Status> getStatoos() {
        return statoos;
    }

    public void setStatoos(List<Status> statoos) {
        this.statoos = statoos;
    }
}
