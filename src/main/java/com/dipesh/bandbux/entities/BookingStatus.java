package com.dipesh.bandbux.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_booking_status")
public class BookingStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "booking_status_id")
    private int id;

    @JoinColumn(name = "book_id", referencedColumnName = "booking_id")
    @ManyToOne
    private BookingDetails bookingDetails;

    @JoinColumn(name = "stat_name", referencedColumnName = "status_name")
    @ManyToOne
    private StatusName statusNames;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BookingDetails getBookingDetails() {
        return bookingDetails;
    }

    public void setBookingDetails(BookingDetails bookingDetails) {
        this.bookingDetails = bookingDetails;
    }

    public StatusName getStatusNames() {
        return statusNames;
    }

    public void setStatusNames(StatusName statusNames) {
        this.statusNames = statusNames;
    }
}