package com.dipesh.bandbux.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_organizer_post")
public class OrganizerPost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "op_id")
    private int opID;
    @JoinColumn(name = "organizer_id", referencedColumnName = "id")
    @ManyToOne
    private User user;
    @JoinColumn(name = "p_id", referencedColumnName = "post_id")
    @ManyToOne
    private Post post;

    public int getOpID() {
        return opID;
    }

    public void setOpID(int opID) {
        this.opID = opID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

}