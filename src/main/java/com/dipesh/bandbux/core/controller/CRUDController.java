package com.dipesh.bandbux.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

public abstract class CRUDController<T, ID> extends SiteController {

    @Autowired
    private JpaRepository<T, ID> repository;

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") ID id, Model model) {
        model.addAttribute("record", repository.findById(id).get());
        return viewPath + "/edit";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") ID id, Model model) {
        repository.deleteById(id);
        return "redirect:/" + viewPath + "?success";
    }
    
    @GetMapping(value = "/json/{id}")
    @ResponseBody
    public T jsonDetail(@PathVariable("id") ID id) {
        return repository.findById(id).get();
    }

    @PostMapping("/json")
    @ResponseBody
    public String save(T model){
        repository.save(model);
        return "success";
    }

    @GetMapping(value = "/table")
    public String table(Model model) {
        model.addAttribute("records", repository.findAll());
        return viewPath + "/table";
    }

    @GetMapping("/json")
    @ResponseBody
    public List<T> showAll(){
        return repository.findAll();
    }
}