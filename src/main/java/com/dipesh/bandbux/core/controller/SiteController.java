package com.dipesh.bandbux.core.controller;

import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.bind.annotation.ModelAttribute;

public abstract class SiteController{

    protected String pageTitle;
    protected String pageIndex;
    protected String pageURI;
    protected String viewPath;

    @ModelAttribute(value = "pageTitle")
    public String getPageTitle() {
        return pageTitle;
    }

    @ModelAttribute(value = "pageURI")
    public String getpageURI() {
        return pageURI;
    }

    @ModelAttribute(value = "viewPath")
    public String getviewPath() {
        return viewPath;
    }

    @ModelAttribute(value = "pageIndex")
    public String getpageIndex() {
        return pageIndex;
    }

}